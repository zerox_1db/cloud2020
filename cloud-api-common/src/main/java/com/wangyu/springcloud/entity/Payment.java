package com.wangyu.springcloud.entity;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * payment 实体
 *
 * AllArgsConstructor 全参构造器
 * NoArgsConstructor 空参构造器
 * @author wangyu
 * @version 1.0
 * @date 2020/10/23 0:28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payment implements Serializable {

  /**
   * ID
   */
  private Long id;

  /**
   * 流水好
   */
  private String serial;
}
