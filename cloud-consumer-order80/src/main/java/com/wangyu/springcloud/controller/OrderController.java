package com.wangyu.springcloud.controller;

import com.wangyu.springcloud.entity.CommonResult;
import com.wangyu.springcloud.entity.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author wangyu
 * @version 1.0
 * @date 2020/10/25 14:29
 */
@Slf4j
@RestController
@RequestMapping("/consumer")
public class OrderController {

  private static final String PAYMENT_URL = "http://localhost:8001";

  @Autowired
  private RestTemplate restTemplate;

  /**
   * 创建
   */
  @GetMapping("/payment/create")
  public CommonResult<Payment> create(@RequestBody Payment payment) {
    return restTemplate.postForObject(PAYMENT_URL + "/payment/create", payment, CommonResult.class);
  }

  /**
   * 获取
   */
  @GetMapping("/payment/get/{id}")
  public CommonResult<Payment> getPayment(@PathVariable("id") Long id) {
    return restTemplate.getForObject(PAYMENT_URL + "/payment/get/" + id, CommonResult.class);
  }
}
