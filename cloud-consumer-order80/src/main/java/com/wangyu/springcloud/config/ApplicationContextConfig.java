package com.wangyu.springcloud.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author wangyu
 * @version 1.0
 * @date 2020/10/25 14:36
 */
@Configuration
public class ApplicationContextConfig {

  /**
   * 引入RestTemplate
   */
  @Bean
  public RestTemplate getRestTemplate() {
    return new RestTemplate();
  }
}
