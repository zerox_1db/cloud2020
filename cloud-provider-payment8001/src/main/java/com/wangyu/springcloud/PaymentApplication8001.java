package com.wangyu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 启动类
 * EnableEurekaClient 注解表示当前服务作为eureka的客户端
 *
 * @author wangyu
 * @version 1.0
 * @date 2020/10/22 1:05
 */
@EnableEurekaClient
@SpringBootApplication
public class PaymentApplication8001 {

    public static void main(String[] args) {
        SpringApplication.run(PaymentApplication8001.class, args);
    }
}
