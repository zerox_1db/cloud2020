package com.wangyu.springcloud.controller;

import com.wangyu.springcloud.entity.CommonResult;
import com.wangyu.springcloud.entity.Payment;
import com.wangyu.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * payment 控制器
 *
 * @author wangyu
 * @version 1.0
 * @date 2020/10/24 1:12
 */
@Slf4j
@RestController
@RequestMapping("/payment")
public class PaymentController {

  @Autowired
  private PaymentService paymentService;

  /**
   * 新增
   */
  @PostMapping("/create")
  public CommonResult create(@RequestBody Payment payment) {
    int result = paymentService.create(payment);
    log.info("****插入结果：" + result);
    if (result > 0) {
      return new CommonResult(200, "插入成功", result);
    } else {
      return new CommonResult(500, "插入失败", null);
    }
  }

  /**
   * 根据ID查询
   */
  @GetMapping("/get/{id}")
  public CommonResult getById(@PathVariable("id") Long id) {
    Payment payment = paymentService.getPaymentById(id);
    if (payment != null) {
      return new CommonResult(200, "操作成功", payment);
    } else {
      return new CommonResult(500, "没有对应记录，查询ID"+id, null);
    }
  }
}
