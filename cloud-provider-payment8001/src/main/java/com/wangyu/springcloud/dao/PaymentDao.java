package com.wangyu.springcloud.dao;

import com.wangyu.springcloud.entity.Payment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author wangyu
 * @date 2020/10/23 0:56
 */
@Mapper
public interface PaymentDao {

  /**
   * 新增
   * @param payment
   * @return int
   */
  int create(Payment payment);

  /**
   * 根据ID查询
   * @param id
   * @return payment
   */
  Payment getPaymentById(@Param("id") Long id);
}
