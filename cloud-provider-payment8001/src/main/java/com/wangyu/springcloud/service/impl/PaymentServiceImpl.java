package com.wangyu.springcloud.service.impl;

import com.wangyu.springcloud.dao.PaymentDao;
import com.wangyu.springcloud.entity.Payment;
import com.wangyu.springcloud.service.PaymentService;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * payment服务层实现
 *
 * @author wangyu
 * @version 1.0
 * @date 2020/10/24 1:01
 */
@Service("paymentService")
public class PaymentServiceImpl implements PaymentService {

  @Resource
  private PaymentDao paymentDao;

  @Override
  public int create(Payment payment) {
    return paymentDao.create(payment);
  }

  @Override
  public Payment getPaymentById(Long id) {
    return paymentDao.getPaymentById(id);
  }
}
