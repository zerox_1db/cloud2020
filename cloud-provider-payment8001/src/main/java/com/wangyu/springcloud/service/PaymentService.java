package com.wangyu.springcloud.service;

import com.wangyu.springcloud.entity.Payment;

/**
 * payment 服务层
 *
 * @author wangyu
 * @date 2020/10/24 1:01
 */
public interface PaymentService {

  /**
   * 新增
   *
   * @param payment payment
   * @return paymentId
   */
  int create(Payment payment);

  /**
   * 根据ID查询
   *
   * @param id id
   * @return payment
   */
  Payment getPaymentById(Long id);
}
