CREATE TABLE payment
(
    id     BIGINT(20)      NOT NULL AUTO_INCREMENT COMMENT 'id',
    serial VARCHAR(200) DEFAULT '',
    primary key (id)
)ENGINE =Innodb AUTO_INCREMENT=1 DEFAULT CHARSET =utf8;
